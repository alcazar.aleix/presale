/*
 * SPDX-License-Identifier: MIT
 */
 //
pragma solidity 0.8.10;
import "./presale_imports.sol";


/* @title Presale for BEP20 Tokens. Various owners with multi-sig functions to withdrawEmergency Tokens and forward funds.*/

contract Presale is Pausable {
    using SafeMath for uint256;
    //The BUD token in this case
    IBEP20 token;//revisar IBEP 20 IERC 20
    //Wallet for BNB funds
    address public walletFunds = 0x57fb6a89fBCcA5931B1BD21A9100935dA8AA3DcF;
    //owner of the contract
    mapping (address => uint256) hoursNextClaim;
    // Amount of BNB users payed
    uint256 bnbRaised;
    uint256 public presale_index = 0;
    uint256 amountFunds;
    // Amount of BNB forwarded to wallet
    uint256 bnbForward;
    bool public finalized;
    bool finalizePending;
    // max cap, min invest, max invest, rate
    uint256 maxCap = 1500000000000000000000;//adjust the maxCap according he zeroes of the token
    uint256 public raisedCap;
    uint256 minInvest = 100000000000000000;
    uint256 maxInvest = 1500000000000000000;
    address _owner;
    address payable walletFundsTemp;
    uint256 amountToDistribute; 
    uint256 public cooldownClaim = 1 days;//change for 1 days in production
    mapping (address => mapping(uint256 => uint256)) public lastClaims;
    mapping (address => mapping(uint256 => uint256)) indexClaims;
    uint256 rate = 500;
    mapping (address => mapping(uint256 => uint256)) public budBalances;
    mapping (address => mapping(uint256 => uint256)) initialBudBalances;
    mapping (address => mapping(uint256 => uint256)) bnbBalances;
    mapping (address => mapping(uint256 => bool)) public pays;
    address[] public owners;
    mapping(address => bool) public isOwner;
    uint public numConfirmationsRequired;
    uint walletConfirmations = 0;

    // mapping from forward index => owner => bool
    mapping(uint => mapping(address => bool)) public isConfirmed;
    mapping(uint => mapping(address => bool)) public isWithdrawConfirmed;
    mapping(address => bool) public isWalletConfirmed;
    struct Transaction {
        address to;
        uint value;
        uint fIndex;
        bool executed;
        uint numConfirmations;
    }
    Transaction[] public transactions;
    Transaction[] public emergencyWithdraws;


    event ConfirmTransaction(address indexed owner, uint indexed txIndex);

    event TransferReceived(address _from, uint _amount);
    event TransferSend(address _from, address _destAddr, uint _amount);
    event Finalized();
    bool internal locked;
    modifier onlyOwners() {
        require(isOwner[msg.sender]== true, "not owner");
        _;
    }
    modifier noReentrant() {
        require(!locked, "No re-entrancy");
        locked = true;
        _;
        locked = false;
    }

   
    constructor(address[] memory _owners, uint _numConfirmationsRequired) payable { 
        require(_owners.length > 0, "owners required");
        require(
            _numConfirmationsRequired > 0 &&
                _numConfirmationsRequired <= _owners.length,
            "invalid number of required confirmations"
        );

        for (uint i = 0; i < _owners.length; i++) {
            address owner = _owners[i];

            require(owner != address(0), "invalid owner");
            require(!isOwner[owner], "owner not unique");

            isOwner[owner] = true;
            owners.push(owner);
        }

        numConfirmationsRequired = _numConfirmationsRequired;
        
    }
    fallback() payable external {
        purchaseTokens(msg.sender,msg.value);
    }
    function purchaseTokens(address _sender, uint _value) public payable  {
        //Only one purchase for each wallet, every transaction more will fail
        require(pays[_sender][presale_index] != true,"Only one purchase for each wallet");
        // The BNB qty must be higher than min invest
        require(_value >= minInvest);
        // The BNB qty must be less than max invest
        require(_value <= maxInvest,"You can't buy more tha the max specified!");
        //The buy qty no more than maxCap
        //require(((msg.value.mul(rate).div(1000000000000))+raisedCap) <= maxCap,"Not enough tokens, try with less quantity!");//adjust de 12 zeroes division according the token
        //Only accept payments when the presale in ongoing. 
        require(!finalizePending,"The Hard Cap is reached");
        require(!finalized,"Presale is finished no accept more BNB");
        if(((_value.mul(rate))+raisedCap) >= maxCap){
            finalizePending = true;
            //enviar event de finalitzacio per pagina presale
        }
        //Assign ready purchased for wallet msg sender
        pays[_sender][presale_index]=true;
        //Assign BNB balance to buyer
        bnbBalances[_sender][presale_index]=_value; 
        //Update BNB raised to forward
        bnbRaised += bnbBalances[_sender][presale_index];
        //Assign BUD balance to buyer
        budBalances[_sender][presale_index]=(bnbBalances[_sender][presale_index].mul(rate)); //dividim per 12 zeros per tal de comparar els bud (6 decimals) amb bnb (18 decimals)
        emit TransferReceived(_sender, _value);
        //Update BUD raised 
        raisedCap += budBalances[_sender][presale_index];
        initialBudBalances[_sender][presale_index] = budBalances[_sender][presale_index];
    }
    function adjust_presale(uint256 _minInvest, uint256 _maxInvest, uint256 _rate,uint256 _maxCap)external onlyOwner {
        require(_minInvest > 0);
        require(_maxInvest > 0);
        require(_maxCap > 0);
        minInvest = _minInvest;
        maxInvest = _maxInvest;
        rate = _rate;//number of tokens for each BNB
        maxCap = _maxCap;
        
    }
    function prepareForward() external noReentrant onlyOwners returns(uint){
        uint _forwardIndex = transactions.length;
        transactions.push(Transaction({
            to: payable(walletFunds),
            value: bnbRaised,
            fIndex: _forwardIndex,
            executed: false,
            numConfirmations: 0
        })
        );
        require(walletFunds != address(0));
        return _forwardIndex;
    }
    function prepareEmergencyWithDraw() external noReentrant onlyOwners returns(uint){
        uint _withdrawIndex = emergencyWithdraws.length;
        emergencyWithdraws.push(Transaction({
            to: payable(walletFunds),
            value: balanceOfTokens(),
            fIndex: _withdrawIndex,
            executed: false,
            numConfirmations: 0
        })
        );
        require(walletFunds != address(0));
        return _withdrawIndex;
    }

    function confirmEmergencyWithdraw(uint _withdrawIndex) external noReentrant onlyOwners{
        Transaction storage withdrawals = emergencyWithdraws[_withdrawIndex];
        require(!isWithdrawConfirmed[_withdrawIndex][msg.sender], "Already confirmed");
        require(withdrawals.to != address(0));
        withdrawals.numConfirmations += 1;
        isWithdrawConfirmed[_withdrawIndex][msg.sender] = true;

        emit ConfirmTransaction(msg.sender, _withdrawIndex);
    }

    function confirmTransaction(uint _forwardIndex) noReentrant external onlyOwners{
        Transaction storage transaction = transactions[_forwardIndex];
        require(transaction.to != address(0),"The transaction go to 0 addres, cancelled");
        require(!isConfirmed[_forwardIndex][msg.sender], "Already confirmed");
        transaction.numConfirmations += 1;
        isConfirmed[_forwardIndex][msg.sender] = true;

        emit ConfirmTransaction(msg.sender, _forwardIndex);
    }

    function confirmWallet() external noReentrant onlyOwners {
        require(!isWalletConfirmed[msg.sender],"Wallet already confirmed");
        isWalletConfirmed[msg.sender] = true;
        walletConfirmations++;
        if(walletConfirmations >= owners.length){
            require(walletFundsTemp != address (0));
            walletFunds = walletFundsTemp;
            walletConfirmations = 0;
            for (uint i = 0; i < owners.length; i++){
                isWalletConfirmed[owners[i]] = false;
            }
        }
    }
    function setWallet(address payable _walletFunds) external onlyOwners{
        require(_walletFunds != address(0), "address 0 ");
        walletFundsTemp = _walletFunds;

    }

    function getOwners() public view returns (address[] memory) {
        return owners;
    }

    function getTransactionCount() public view returns (uint) {
        return transactions.length;
    }

    function getTransaction(uint _forwardIndex) public view returns (address to,uint value,uint fIndex, bool executed, uint numConfirmations){
        Transaction storage transaction = transactions[_forwardIndex];

        return (
            transaction.to,
            transaction.value,
            transaction.fIndex,
            transaction.executed,
            transaction.numConfirmations
        );
    }


    function adjustToken(IBEP20 _token)public onlyOwners {
        token = _token;
    }
    
    function changeCooldown(uint256 _cooldownClaim) external payable onlyOwners returns(uint){
        cooldownClaim = _cooldownClaim;
        return cooldownClaim;
    }
    function balanceOfTokens() public view returns(uint256){
        return token.balanceOf(address(this));
    }
    function isPayed() internal view returns(bool){
        return pays[msg.sender][presale_index];
    }

    function balanceBNB() public view returns(uint256){
        return address(this).balance;
    }

    function forwardFunds(uint _forwardIndex) external noReentrant onlyOwners{
        Transaction storage transaction = transactions[_forwardIndex];
        require(transaction.to != address(0));
        require(!transaction.executed,"Transaction already executed");
        require(transaction.numConfirmations >= numConfirmationsRequired,"Not all confirmations yet");
        uint256 forwardAmount = bnbRaised;
        bnbForward += bnbRaised;
        bnbRaised = 0;
        transaction.executed = true;
        (bool success,) = transaction.to.call{value: balanceBNB()}("");
        require(success, "Failed to send funds");
        emit TransferSend(address(this), payable(walletFunds),forwardAmount);

    }
    function emergencyWithdrawTokens(uint _withdrawIndex) external noReentrant onlyOwners {
        Transaction storage withdrawals = emergencyWithdraws[_withdrawIndex];
        require(withdrawals.to != address(0));
        require(!withdrawals.executed, "Emergency withdraw tokens already executed");
        require(withdrawals.numConfirmations >= numConfirmationsRequired, "Not all confirmations yet");
        token.approve(address(this),balanceOfTokens());
        token.transferFrom(address(this),withdrawals.to,balanceOfTokens());
        emit TransferSend(address(this), withdrawals.to,balanceOfTokens());
    }

    function isPresaleFinished() internal view returns(bool){
        if(raisedCap >= maxCap){
            return true;
        }else{
            return false;
        }
    }
    function finishPresale() external onlyOwners{
        require(isPresaleFinished(),"Presale no finished, wait to finalize after the maxCap is reached");
        require(!finalized,"Presale already closed before");
        emit Finalized();
        finalized = true;

    }
    function canClaim(address _claimer) public view returns(bool){
        require(budBalances[_claimer][presale_index] != 0, "Can't claim");
        if(block.timestamp >= (lastClaims[_claimer][presale_index] + cooldownClaim)){
            return true;
        }else{
            return false;
        }
    }
    function claimTokens() external noReentrant whenNotPaused returns(uint){
        require(msg.sender != address(0));
        require(finalized, "Presale no finalized");
        require(budBalances[msg.sender][presale_index] > 0, "You don'thave funds to claim");
        require(block.timestamp >= (lastClaims[msg.sender][presale_index] + cooldownClaim), "Please wait, you can claim only 1 time per day");
        lastClaims[msg.sender][presale_index] = block.timestamp;   
        if(indexClaims[msg.sender][presale_index] == 0){
        // Calculate the percentage with initial BUD Balance
           amountToDistribute = initialBudBalances[msg.sender][presale_index].div(5); 
          
           if(budBalances[msg.sender][presale_index] < amountToDistribute){
               amountToDistribute = budBalances[msg.sender][presale_index];
           }else{
               amountToDistribute = initialBudBalances[msg.sender][presale_index].div(5); 
           }
           //update bud balances
           budBalances[msg.sender][presale_index] = budBalances[msg.sender][presale_index] - amountToDistribute;
        }else{
            // Calculate the percentage with initial BUD Balance
            amountToDistribute = initialBudBalances[msg.sender][presale_index].div(20);

            if(budBalances[msg.sender][presale_index] < amountToDistribute){
               amountToDistribute = budBalances[msg.sender][presale_index];
            }else{
               amountToDistribute = initialBudBalances[msg.sender][presale_index].div(20); 
            }       

            budBalances[msg.sender][presale_index] = budBalances[msg.sender][presale_index] - amountToDistribute;
        }
        indexClaims[msg.sender][presale_index] += 1;
        //Approve spender
        token.approve(address(this),amountToDistribute);
        token.transferFrom(address(this),msg.sender,amountToDistribute);
        emit TransferSend(address(this), msg.sender,amountToDistribute);
        return amountToDistribute;
    }
    function resetPresaleBalances() external onlyOwners {
        presale_index ++;
        finalized = false;
        finalizePending = false;
        raisedCap = 0;
        
    }

}
